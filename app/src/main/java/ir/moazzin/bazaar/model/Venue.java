package ir.moazzin.bazaar.model;

import androidx.annotation.NonNull;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "venues")
public class Venue extends BaseObservable {

    public static DiffUtil.ItemCallback<Venue> DIFF_CALLBACK = new DiffUtil.ItemCallback<Venue>() {
        @Override
        public boolean areItemsTheSame(@NonNull Venue oldItem, @NonNull Venue newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Venue oldItem, @NonNull Venue newItem) {
            return oldItem.getId().equals(newItem.getId());
        }
    };

    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    private String source;

    @Bindable
    public String getId() {
        return id;
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}