package ir.moazzin.bazaar.net;

import ir.moazzin.bazaar.model.DetailResponse;
import ir.moazzin.bazaar.model.VenuesResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static ir.moazzin.bazaar.net.ServiceGenerator.HEADER_CACHE;

public interface FoursquareServiceInterface {

    @GET("venues/explore")
    @Headers(HEADER_CACHE + ": 3600") //مدت زمان کش کردن بر حسب ثانیه
    Call<VenuesResponse> getVenues(@Query("ll") String location, @Query("offset") int offset, @Query("limit") int limit, @Query("sortByDistance") int sortByDistance);

    @GET("venues/{venueId}")
    @Headers(HEADER_CACHE + ": 3600") //مدت زمان کش کردن بر حسب ثانیه
    Call<DetailResponse> getVenueDetail(@Path("venueId") String venueId);
}