package ir.moazzin.bazaar.net;

import com.google.gson.reflect.TypeToken;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import ir.moazzin.bazaar.model.GeneralResponse;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;


public class FacilitatorConverterFactory extends Converter.Factory {

    private FacilitatorConverterFactory() {}

    static FacilitatorConverterFactory create() {
        return new FacilitatorConverterFactory();
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {

        Type envelopedType = TypeToken.getParameterized(GeneralResponse.class, type).getType();

        Converter<ResponseBody, GeneralResponse<?>> delegate = retrofit.nextResponseBodyConverter(this, envelopedType, annotations);

        return (Converter<ResponseBody, Object>) body -> {
            GeneralResponse<?> generalResponse = delegate.convert(body);
            if (generalResponse != null) {
                return generalResponse.response;
            }
            return null;
        };
    }
}