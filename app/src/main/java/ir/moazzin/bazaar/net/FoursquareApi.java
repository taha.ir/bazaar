package ir.moazzin.bazaar.net;

import ir.moazzin.bazaar.model.DetailResponse;
import ir.moazzin.bazaar.model.VenuesResponse;
import retrofit2.Call;

// singleton with enum (Effective Java Book)
public enum FoursquareApi {
    INSTANCE;

    public Call<VenuesResponse> getVenues(String location, int offset, int limit) {
        return ServiceGenerator.getFoursquareService().getVenues(location, offset, limit, 1);
    }

    public Call<DetailResponse> getVenueDetail(String venueId) {
        return ServiceGenerator.getFoursquareService().getVenueDetail(venueId);
    }
}