package ir.moazzin.bazaar.net;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;

public class ServiceGenerator {

    public static final String HEADER_CACHE = "android-cache";

    private static final String FOURSQUARE_BASE_URL = "https://api.foursquare.com/v2/";
    private static final String FOURSQUARE_CLIENT_ID = "PNSQGCCEIRH4NYHULPP1RGB1KSGLJK11BKLP42E5ONFP2EDH";
    private static final String FOURSQUARE_CLIENT_SECRET = "K2JIHBGA4RODM1EJ5435VACDNYHX5CG2323DQGUL12POQMB4";
    private static final String CACHE_DIR = "httpCache";
    private static final long CACHE_SIZE = 10 * 1024 * 1024;

    private static FoursquareServiceInterface foursquareService;
    private static File appCacheDirectory;

    public static FoursquareServiceInterface getFoursquareService() {
        if (foursquareService == null) {
            if (appCacheDirectory == null) {
                throw new RuntimeException ("must set application ache directory");
            }
            createFoursquareService();
        }
        return foursquareService;
    }

    private static void createFoursquareService() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.level(BODY);

        //foursquare api key chain
        Interceptor foursquareApiKeyInterceptor = chain -> {
            Request originalRequest = chain.request();
            HttpUrl originalHttpUrl = originalRequest.url();

            HttpUrl url = originalHttpUrl.newBuilder()
                    .addQueryParameter("client_id", FOURSQUARE_CLIENT_ID)
                    .addQueryParameter("client_secret", FOURSQUARE_CLIENT_SECRET)
                    .addQueryParameter("v", "20190425")
                    .build();

            Request.Builder requestBuilder = originalRequest.newBuilder()
                    .url(url);

            Request request = requestBuilder.build();
            return chain.proceed(request);
        };

        //cache chain
        Interceptor httpCacheInterceptor = chain -> {
            Request originalRequest = chain.request();

            if (originalRequest.header(HEADER_CACHE) != null) {
                Request offlineRequest = originalRequest.newBuilder()
                        .header("Cache-Control", "only-if-cached, "
                                + "max-stale=" + originalRequest.header(HEADER_CACHE))
                        .build();
                Response response = chain.proceed(offlineRequest);
                if (response.isSuccessful()) {
                    return response;
                }
            }
            return chain.proceed(originalRequest);
        };

        File httpCacheDirectory = new File(appCacheDirectory, CACHE_DIR);
        Cache cache = new Cache(httpCacheDirectory, CACHE_SIZE);
        OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder()
                        .cache(cache)
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .addInterceptor(logging)
                        .addInterceptor(foursquareApiKeyInterceptor)
                        .addInterceptor(httpCacheInterceptor);

        Retrofit.Builder retrofitBuilder =
                new Retrofit.Builder()
                        .baseUrl(FOURSQUARE_BASE_URL)
                        .client(httpClient.build())
                        .addConverterFactory(FacilitatorConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = retrofitBuilder.build();

        foursquareService = retrofit.create(FoursquareServiceInterface.class);
    }

    public static void setAppCacheDirectory(File appCacheDirectory) {
        ServiceGenerator.appCacheDirectory = appCacheDirectory;
    }
}