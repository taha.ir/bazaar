package ir.moazzin.bazaar;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ir.moazzin.bazaar.domain.VenueDetailViewModel;
import ir.moazzin.bazaar.model.Detail;

public class VenueDetailFragment extends Fragment {

    private static final String VENUE_ID = "VENUE_ID";

    public static VenueDetailFragment newInstance(String venueId) {
        VenueDetailFragment instance =  new VenueDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(VENUE_ID, venueId);
        instance.setArguments(bundle);
        return instance;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_venue_detail, container, false);
        TextView mNameText = view.findViewById(R.id.venue_name);
        TextView mDescriptionText = view.findViewById(R.id.venue_description);
        Observer<Detail> observer = detail -> {
            mNameText.setText(detail.getName());
            if (TextUtils.isEmpty(detail.getDescription())) {
                mDescriptionText.setText("Description not exist");
            } else {
                mDescriptionText.setText(detail.getDescription());
            }
        };
        String venueId = getArguments().getString(VENUE_ID, "");
        if (!TextUtils.isEmpty(venueId)) {
            VenueDetailViewModel mViewModel = ViewModelProviders.of(this).get(VenueDetailViewModel.class);
            mViewModel.getDetail(venueId).observeForever(observer);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
