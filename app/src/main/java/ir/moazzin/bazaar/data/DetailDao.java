package ir.moazzin.bazaar.data;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import ir.moazzin.bazaar.model.Detail;
import ir.moazzin.bazaar.model.Venue;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface DetailDao {
    @Insert(onConflict = REPLACE)
    void save(Detail detail);

    @Query("SELECT * FROM venues WHERE id = :id")
    Detail load(String id);
}