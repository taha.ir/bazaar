package ir.moazzin.bazaar.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import ir.moazzin.bazaar.model.Detail;
import ir.moazzin.bazaar.model.Venue;


@Database(entities = {Venue.class, Detail.class}, version = 3)
public abstract class FoursquareDatabase extends RoomDatabase {
    private static final String DB_NAME = "foursquare_db";
    private static final Object sLock = new Object();
    private static FoursquareDatabase instance;

    public abstract VenueDao venueDao();
    public abstract DetailDao detailDao();

    public static FoursquareDatabase getInstance(Context context) {
        synchronized (sLock) {
            if (instance == null) {
                instance = Room.databaseBuilder(
                        context.getApplicationContext(), FoursquareDatabase.class, DB_NAME)
                        .fallbackToDestructiveMigration()
                        .build();
            }
            return instance;
        }
    }

}