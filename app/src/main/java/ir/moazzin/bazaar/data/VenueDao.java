package ir.moazzin.bazaar.data;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import ir.moazzin.bazaar.model.Venue;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface VenueDao {
    @Insert(onConflict = REPLACE)
    void save(Venue venue);

    @Query("SELECT * FROM venues")
    List<Venue> loadAll();

    @Query("DELETE FROM venues")
    void deleteAll();

    @Query("SELECT * FROM venues WHERE source = :source")
    List<Venue> loadWithSource(String source);
}