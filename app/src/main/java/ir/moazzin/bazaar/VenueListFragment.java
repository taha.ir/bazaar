package ir.moazzin.bazaar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.location.LocationResult;

import java.util.Objects;

import ir.moazzin.bazaar.model.Venue;
import ir.moazzin.bazaar.domain.VenuesListViewModel;
import ir.moazzin.bazaar.domain.VenuesPagedListAdapter;

import static android.content.Context.MODE_PRIVATE;
import static ir.moazzin.bazaar.MainActivity.LAST_VISIT_LOCATION_KEY;
import static ir.moazzin.bazaar.MainActivity.LOCALE_REQUEST_ACTION;
import static ir.moazzin.bazaar.MainActivity.MAIN_PREFERENCES_NAME;

public class VenueListFragment extends Fragment {

    private OnVenueSelectListener mListener;
    private IntentFilter mIntentFilter = new IntentFilter(LOCALE_REQUEST_ACTION);
    private RecyclerView recyclerView;
    private VenuesListViewModel viewModel;
    private Location mLastVisitedLocation;

    public VenueListFragment() {
    }

    static VenueListFragment newInstance() {
        return new VenueListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_venue_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            viewModel = ViewModelProviders.of(getActivity()).get(VenuesListViewModel.class);
        }
        return view;
    }

    private void observersRegisters(String location) {
        VenuesPagedListAdapter pageListAdapter = new VenuesPagedListAdapter(mListener);
        viewModel.getVenues(location).observe(this, pageListAdapter::submitList);
        viewModel.getNetworkState().observe(this, pageListAdapter::setNetworkState);
        recyclerView.setAdapter(pageListAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        retrieveLastVisitLocation();
        if (context instanceof OnVenueSelectListener) {
            mListener = (OnVenueSelectListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
        context.registerReceiver(mBroadcastReceiver, mIntentFilter);
    }

    @Override
    public void onDetach() {
        Objects.requireNonNull(getActivity()).unregisterReceiver(mBroadcastReceiver);
        storeLastVisitLocation();
        super.onDetach();
        mListener = null;
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (LocationResult.hasResult(intent)) {
                LocationResult locationResult = LocationResult.extractResult(intent);
                Location location = locationResult.getLastLocation();
                if (location != null) {
                    Toast.makeText(getActivity(), "real location:" + location.getLatitude() + "," + location.getLongitude(), Toast.LENGTH_LONG).show();
                    if (mLastVisitedLocation == null || location.distanceTo(mLastVisitedLocation) > 100) {
                        mLastVisitedLocation = location;
                    }
                    String strLocation = mLastVisitedLocation.getLatitude() + "," + mLastVisitedLocation.getLongitude();
                    Toast.makeText(getActivity(), "used location:" + strLocation, Toast.LENGTH_LONG).show();
                    observersRegisters(strLocation);
                } else {
                    Toast.makeText(getActivity(), "*** location object is null ***", Toast.LENGTH_SHORT).show();
                }
            }
        }

    };

    public interface OnVenueSelectListener {
        void onVenueSelect(Venue venue);
    }

    private void retrieveLastVisitLocation() {
        String locationStr = getActivity().getSharedPreferences(MAIN_PREFERENCES_NAME, MODE_PRIVATE).getString(LAST_VISIT_LOCATION_KEY, "");
        if (!TextUtils.isEmpty(locationStr)) {
            String[] locationParts = locationStr.split(",");
            mLastVisitedLocation = new Location(locationParts[0]);
            mLastVisitedLocation.setLongitude(Double.parseDouble(locationParts[1]));
            mLastVisitedLocation.setLatitude(Double.parseDouble(locationParts[2]));
            mLastVisitedLocation.setAltitude(Double.parseDouble(locationParts[3]));
        }
    }

    private void storeLastVisitLocation() {
        if (mLastVisitedLocation != null) {
            getActivity().getSharedPreferences(MAIN_PREFERENCES_NAME, MODE_PRIVATE)
                    .edit()
                    .putString(LAST_VISIT_LOCATION_KEY,
                            mLastVisitedLocation.getProvider() + "," +
                                    mLastVisitedLocation.getLongitude() + "," +
                                    mLastVisitedLocation.getLatitude() + "," +
                                    mLastVisitedLocation.getAltitude())
                    .apply();
        }
    }
}
