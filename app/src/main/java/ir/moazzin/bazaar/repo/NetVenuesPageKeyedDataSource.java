package ir.moazzin.bazaar.repo;

import android.database.Observable;
import android.os.Handler;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.PageKeyedDataSource;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import ir.moazzin.bazaar.data.NetworkState;
import ir.moazzin.bazaar.model.Venue;
import ir.moazzin.bazaar.model.VenuesResponse;
import ir.moazzin.bazaar.net.FoursquareApi;
import ir.moazzin.bazaar.net.FoursquareServiceInterface;
import ir.moazzin.bazaar.net.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class NetVenuesPageKeyedDataSource extends PageKeyedDataSource<Integer, Venue> {

    static final int LOADING_PAGE_SIZE = 20;

    private final MutableLiveData networkState;
    private final MutableLiveData<List<Venue>> venues;
    private String mLocation;

    NetVenuesPageKeyedDataSource() {
        networkState = new MutableLiveData();
        venues = new MutableLiveData<>();
    }

    MutableLiveData getNetworkState() {
        return networkState;
    }

    MutableLiveData<List<Venue>> getVenues() {
        return venues;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Venue> callback) {
        if (TextUtils.isEmpty(mLocation)) {
            networkState.postValue(new NetworkState(NetworkState.Status.FAILED, "Location is empty"));
            callback.onResult(new ArrayList<>(), 0, 1);
            return;
        }
        networkState.postValue(NetworkState.LOADING);
        FoursquareApi.INSTANCE.getVenues(mLocation, 0, LOADING_PAGE_SIZE).enqueue(new Callback<VenuesResponse>() {
            @Override
            public void onResponse(Call<VenuesResponse> call, Response<VenuesResponse> response) {
                if (response.isSuccessful()) {
                    List<Venue> venueList = new ArrayList<>();
                    for (VenuesResponse.Item i: response.body().getGroups().get(0).getItems()) {
                        venueList.add(i.getVenue());
                    }
                    callback.onResult(venueList, 0, 1);
                    networkState.postValue(NetworkState.LOADED);
                    for (Venue v: venueList) {
                        v.setSource(mLocation);
                    }
                    venues.postValue(venueList);// آبزروبلی را برای ذخیره اطلاعت آپدیت می کنیم
                } else {
                    networkState.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                    callback.onResult(new ArrayList<>(), 0, 1);
                }
            }

            @Override
            public void onFailure(Call<VenuesResponse> call, Throwable t) {
                String errorMessage = t.getMessage() == null ? "unknown error" : t.getMessage();
                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorMessage));
                callback.onResult(new ArrayList<>(), 0, 1);
            }
        });

    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Venue> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Venue> callback) {
        final AtomicInteger page = new AtomicInteger(params.key);
        if (TextUtils.isEmpty(mLocation)) {
            networkState.postValue(new NetworkState(NetworkState.Status.FAILED, "location is empty"));
            callback.onResult(new ArrayList<>(), page.get());
            return;
        }
        networkState.postValue(NetworkState.LOADING);
        FoursquareApi.INSTANCE.getVenues(mLocation, page.get(), LOADING_PAGE_SIZE).enqueue(new Callback<VenuesResponse>() {
            @Override
            public void onResponse(Call<VenuesResponse> call, Response<VenuesResponse> response) {
                if (response.isSuccessful()) {
                    List<Venue> venueList = new ArrayList<>();
                    for (VenuesResponse.Item i: response.body().getGroups().get(0).getItems()) {
                        venueList.add(i.getVenue());
                    }
                    callback.onResult(venueList, page.get()+1);
                    networkState.postValue(NetworkState.LOADED);
                    for (Venue v: venueList) {
                        v.setSource(mLocation);
                    }
                    venues.postValue(venueList);
                } else {
                    networkState.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                    callback.onResult(new ArrayList<>(), page.get());
                }
            }

            @Override
            public void onFailure(Call<VenuesResponse> call, Throwable t) {
                String errorMessage = t.getMessage() == null ? "unknown error" : t.getMessage();
                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorMessage));
                callback.onResult(new ArrayList<>(), page.get());
            }
        });
    }

    void setLocation(String location) {
        mLocation = location;
    }
}
