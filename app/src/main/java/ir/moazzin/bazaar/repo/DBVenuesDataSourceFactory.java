package ir.moazzin.bazaar.repo;

import androidx.annotation.NonNull;
import androidx.paging.DataSource;

import ir.moazzin.bazaar.data.VenueDao;

class DBVenuesDataSourceFactory extends DataSource.Factory{
    private DBVenuesPageKeyedDataSource venuesPageKeyedDataSource;

    DBVenuesDataSourceFactory(VenueDao venueDao) {
        venuesPageKeyedDataSource = new DBVenuesPageKeyedDataSource(venueDao);
    }

    @NonNull
    @Override
    public DataSource create() {
        return venuesPageKeyedDataSource;
    }

    public void setSource(String source) {
        venuesPageKeyedDataSource.setSource(source);
    }
}
