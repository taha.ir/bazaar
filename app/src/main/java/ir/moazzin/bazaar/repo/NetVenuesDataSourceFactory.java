package ir.moazzin.bazaar.repo;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

import java.util.List;

import ir.moazzin.bazaar.model.Venue;

class NetVenuesDataSourceFactory extends DataSource.Factory {

    private MutableLiveData<NetVenuesPageKeyedDataSource> networkStatus;
    private NetVenuesPageKeyedDataSource venuePageKeyedDataSource;

    NetVenuesDataSourceFactory() {
        this.networkStatus = new MutableLiveData<>();
        venuePageKeyedDataSource = new NetVenuesPageKeyedDataSource();
    }

    @NonNull
    @Override
    public DataSource create() {
        networkStatus.postValue(venuePageKeyedDataSource);
        return venuePageKeyedDataSource;
    }

    MutableLiveData<NetVenuesPageKeyedDataSource> getNetworkStatus() {
        return networkStatus;
    }

    MutableLiveData<List<Venue>> getVenues() {
        return venuePageKeyedDataSource.getVenues();
    }

    void setLocation(String location) {
        venuePageKeyedDataSource.setLocation(location);
    }
}
