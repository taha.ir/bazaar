package ir.moazzin.bazaar.repo;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.paging.PagedList;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ir.moazzin.bazaar.data.NetworkState;
import ir.moazzin.bazaar.model.Detail;
import ir.moazzin.bazaar.model.DetailResponse;
import ir.moazzin.bazaar.model.Venue;
import ir.moazzin.bazaar.net.FoursquareApi;
import ir.moazzin.bazaar.net.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeneralRepository {
    private static GeneralRepository instance;
    final private VenuesNetwork network;
    final private VenuesDatabase database;
    final private MediatorLiveData liveDataMerger;
    private String mLocation = "";

    private GeneralRepository(Context context) {

        ServiceGenerator.setAppCacheDirectory(context.getApplicationContext().getCacheDir());

        NetVenuesDataSourceFactory dataSourceFactory = new NetVenuesDataSourceFactory();

        network = new VenuesNetwork(dataSourceFactory, boundaryCallback);
        database = new VenuesDatabase(context);
        liveDataMerger = new MediatorLiveData<>();


        Executor executor = Executors.newSingleThreadExecutor();
        // save into db
        Observer<List<Venue>> observer = venues -> executor.execute(() -> {
            for (Venue v: venues) {
                database.save(v);
            }
        });
        dataSourceFactory.getVenues().observeForever(observer);

    }

    private PagedList.BoundaryCallback<Venue> boundaryCallback = new PagedList.BoundaryCallback<Venue>() {
        @Override
        public void onZeroItemsLoaded() {
            super.onZeroItemsLoaded();//اگر گوشی آفلاین باشد و کش http هم خالی باشد از دیتای دیتابیس لود می کند
            liveDataMerger.addSource(database.getVenues(mLocation), value -> {
                liveDataMerger.setValue(value);
                liveDataMerger.removeSource(database.getVenues(mLocation));
            });
        }
    };

    public static GeneralRepository getInstance(Context context){
        if(instance == null){
            instance = new GeneralRepository(context);
        }
        return instance;
    }

    public LiveData<PagedList<Venue>> getPagedVenues(String location) {
        liveDataMerger.removeSource(network.getVenues(mLocation));
        mLocation = location;
        liveDataMerger.addSource(network.getVenues(mLocation), liveDataMerger::setValue);
        return  liveDataMerger;
    }

    public LiveData<NetworkState> getNetworkState() {
        return network.getNetworkState();
    }

    public LiveData<Detail> getDetail(String venueId) {
        MutableLiveData<Detail> result = new MutableLiveData<>();
        FoursquareApi.INSTANCE.getVenueDetail(venueId).enqueue(new Callback<DetailResponse>() {
            @Override
            public void onResponse(Call<DetailResponse> call, Response<DetailResponse> response) {
                Detail detail = response.body().getVenue();
                result.postValue(detail);
                Executor executor = Executors.newSingleThreadExecutor();
                // save into db
                executor.execute(() -> database.save(detail));
            }

            @Override
            public void onFailure(Call<DetailResponse> call, Throwable t) {
                Detail detail = database.getDetail(venueId);
                if (detail != null) {
                    result.postValue(detail);
                }
            }
        });
        return result;
    }
}