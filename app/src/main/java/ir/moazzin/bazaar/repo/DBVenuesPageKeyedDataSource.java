package ir.moazzin.bazaar.repo;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import java.util.List;

import ir.moazzin.bazaar.data.VenueDao;
import ir.moazzin.bazaar.model.Venue;

class DBVenuesPageKeyedDataSource extends PageKeyedDataSource<Integer, Venue> {
    private final VenueDao mVenueDao;
    private String mLocation;

    DBVenuesPageKeyedDataSource(VenueDao venueDao) {
        mVenueDao = venueDao;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Venue> callback) {
        List<Venue> venues = mVenueDao.loadWithSource(mLocation);
        if(venues.size() != 0) {
            callback.onResult(venues, 0, 1);
        }
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Venue> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Venue> callback) {

    }

    void setSource(String location) {
        mLocation = location;
    }
}
