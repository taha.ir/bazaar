package ir.moazzin.bazaar.repo;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import ir.moazzin.bazaar.data.NetworkState;
import ir.moazzin.bazaar.model.Venue;

import static ir.moazzin.bazaar.repo.NetVenuesPageKeyedDataSource.LOADING_PAGE_SIZE;

class VenuesNetwork {
//    private static final int NUMBERS_OF_THREADS = 3;
    private final LiveData<PagedList<Venue>> venuesPaged;
    private final LiveData<NetworkState> networkState;
    private final NetVenuesDataSourceFactory mDataSourceFactory;

    VenuesNetwork(NetVenuesDataSourceFactory dataSourceFactory, PagedList.BoundaryCallback<Venue> boundaryCallback) {
        mDataSourceFactory = dataSourceFactory;
        PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                .setInitialLoadSizeHint(LOADING_PAGE_SIZE).setPageSize(LOADING_PAGE_SIZE).build();

        networkState = Transformations.switchMap(dataSourceFactory.getNetworkStatus(),
                (Function<NetVenuesPageKeyedDataSource, LiveData<NetworkState>>)
                        NetVenuesPageKeyedDataSource::getNetworkState);

//        Executor executor = Executors.newFixedThreadPool(NUMBERS_OF_THREADS);
        LivePagedListBuilder<Integer, Venue> livePagedListBuilder = new LivePagedListBuilder(dataSourceFactory, pagedListConfig);
        venuesPaged = livePagedListBuilder.
//                        setFetchExecutor(executor).
                        setBoundaryCallback(boundaryCallback).
                        build();
    }

    LiveData<PagedList<Venue>> getVenues(String location) {
        mDataSourceFactory.setLocation(location);
        return venuesPaged;
    }

    LiveData<NetworkState> getNetworkState() {
        return networkState;
    }
}
