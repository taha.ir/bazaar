package ir.moazzin.bazaar.repo;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ir.moazzin.bazaar.data.DetailDao;
import ir.moazzin.bazaar.data.FoursquareDatabase;
import ir.moazzin.bazaar.data.VenueDao;
import ir.moazzin.bazaar.model.Detail;
import ir.moazzin.bazaar.model.Venue;

class VenuesDatabase {

    private static final int NUMBERS_OF_THREADS = 3;
    private final VenueDao venueDao;
    private final DBVenuesDataSourceFactory mDataSourceFactory;
    private final DetailDao detailDao;
    private LiveData<PagedList<Venue>> venuesPaged;

    public VenuesDatabase(Context context) {
        FoursquareDatabase db = FoursquareDatabase.getInstance(context);
        venueDao = db.venueDao();
        detailDao = db.detailDao();

        PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                .setInitialLoadSizeHint(Integer.MAX_VALUE).setPageSize(Integer.MAX_VALUE).build();

        Executor executor = Executors.newFixedThreadPool(NUMBERS_OF_THREADS);
        mDataSourceFactory = new DBVenuesDataSourceFactory(venueDao);
        LivePagedListBuilder<Integer, Venue> livePagedListBuilder = new LivePagedListBuilder(mDataSourceFactory, pagedListConfig);
        venuesPaged = livePagedListBuilder.
                setFetchExecutor(executor).
        build();
    }

    LiveData<PagedList<Venue>> getVenues(String source) {
        mDataSourceFactory.setSource(source);
        return venuesPaged;
    }

    void save(Venue venue) {
        venueDao.save(venue);
    }

    Detail getDetail(String venueId) {
        return detailDao.load(venueId);
    }

    void save(Detail detail) {
        detailDao.save(detail);
    }

//    void clean() {
//        venueDao.deleteAll();
//    }
}
