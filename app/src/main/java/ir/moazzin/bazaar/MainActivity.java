package ir.moazzin.bazaar;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnSuccessListener;

import ir.moazzin.bazaar.model.Venue;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;
import static com.google.android.gms.location.LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
import static com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY;

public class MainActivity extends AbstractMainActivity implements VenueListFragment.OnVenueSelectListener {

    static final int LOCATION_REQUEST_CODE = 90;
    static final String LOCALE_REQUEST_ACTION = "locale_request_action";
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    private static final float SMALLEST_DISPLACEMENT_IN_METERS = 100;
    static final String MAIN_PREFERENCES_NAME = "main_preferences";
    static final String LAST_VISIT_LOCATION_KEY = "last_visit_location_longitude";
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
//    private LocationCallback mLocationCallback;
    private PendingIntent mLocationPendingIntent;
    private LocationSettingsRequest mLocationSettingsRequest;
    private TextView generalMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        generalMessage = findViewById(R.id.generalMessage);

        if (savedInstanceState == null) {
            initLocationUpdates();
        }
    }

    private void initLocationUpdates() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        Intent locationIntent = new Intent(LOCALE_REQUEST_ACTION);
        mLocationPendingIntent = PendingIntent.getBroadcast(this, LOCATION_REQUEST_CODE, locationIntent, FLAG_CANCEL_CURRENT);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setSmallestDisplacement(SMALLEST_DISPLACEMENT_IN_METERS);
        mLocationRequest.setPriority(PRIORITY_BALANCED_POWER_ACCURACY); //PRIORITY_HIGH_ACCURACY

        mLocationSettingsRequest = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest)
                .build();
    }

    private void showMessage(String msg) {
        generalMessage.setText(msg);
    }

    @Override
    public void stopLocationUpdates() {
        if (mFusedLocationClient != null) {
            mFusedLocationClient
                    .removeLocationUpdates(mLocationPendingIntent)
                    .addOnCompleteListener(this, task -> {
                        Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                    });
        }
    }

    @Override
    public void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    @SuppressLint("MissingPermission")
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

                        showVenueList();

                        Toast.makeText(MainActivity.this.getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission

                        new Handler().postDelayed(() ->
                            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationPendingIntent), 100);
                    }
                })
                .addOnFailureListener(this, e -> {
                    int statusCode = ((ApiException) e).getStatusCode();
                    switch (statusCode) {
                        case LocationSettingsStatusCodes
                                .RESOLUTION_REQUIRED:
                            showMessage("Location settings are not satisfied. Attempting to upgrade " +
                                    "location settings ");
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                ResolvableApiException rae = (ResolvableApiException) e;
                                rae.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException sie) {
                                showMessage("PendingIntent unable to execute request.");
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            String errorMessage = "Location settings are inadequate, and cannot be " +
                                    "fixed here. Fix in Settings.";
                            showMessage(errorMessage);

                            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                    }

                    //todo update ui
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                if (resultCode == RESULT_CANCELED) {
                    //todo Alert And finish
                    finish();
//                    case Activity.RESULT_OK:
//                        Log.e(TAG, "User agreed to make required location settings changes.");
//                        // Nothing to do. startLocationupdates() gets called in onResume again.
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Log.e(TAG, "User chose not to make required location settings changes.");
//                        mRequestingLocationUpdates = false;
//                        break;
                }
                break;
        }
    }

    private void showVenueList() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, VenueListFragment.newInstance())
                .commit();
    }

    @Override
    public void onVenueSelect(Venue venue) {
        VenueDetailFragment detailsFragment = VenueDetailFragment.newInstance(venue.getId());
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, detailsFragment)
                .addToBackStack(null)
                .commit();
    }
}
