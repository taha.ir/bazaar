package ir.moazzin.bazaar.domain;

import android.graphics.ColorSpace;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.moazzin.bazaar.R;
import ir.moazzin.bazaar.VenueListFragment;
import ir.moazzin.bazaar.data.NetworkState;
import ir.moazzin.bazaar.model.Venue;

class ViewHolderFactory {
    static AbstractViewHolder create(LayoutInflater inflater, ViewGroup parent, int layoutId) {

        View view = inflater.inflate(layoutId, parent, false);

        if (layoutId == R.layout.fragment_venue_row) {
            return new AbstractViewHolder<Venue>(view) {

                private TextView mNameView;
                private TextView mNumberView;
                private Venue venue;

                @Override
                protected void init(View itemView) {
                    mNumberView = view.findViewById(R.id.item_number);
                    mNameView = view.findViewById(R.id.item_name);
                    View mContainerView = view.findViewById(R.id.item_container);
                    mContainerView.setOnClickListener(v -> getListener().onVenueSelect(venue));
                }

                @Override
                public void bind(Venue venue, int position) {
                    this.venue = venue;
                    mNameView.setText(venue.getName());
                    mNumberView.setText(String.valueOf(position));
                    if (position % 2 == 0) {
                        view.setBackgroundColor(0XFFFFEB3B);
                    } else {
                        view.setBackgroundColor(0XFFFFFFFF);
                    }
                }
            };

        } else if (layoutId == R.layout.network_state_row) {
            return new AbstractViewHolder<NetworkState>(view) {

                private TextView errorMsg;
                private ProgressBar progressBar;

                @Override
                protected void init(View itemView) {
                    progressBar = itemView.findViewById(R.id.progress_bar);
                    errorMsg = itemView.findViewById(R.id.error_msg);
                }

                @Override
                public void bind(NetworkState networkState, int position) {
                    if (networkState != null && networkState.getStatus() == NetworkState.Status.RUNNING) {
                        progressBar.setVisibility(View.VISIBLE);
                    } else {
                        progressBar.setVisibility(View.GONE);
                    }

                    if (networkState != null && networkState.getStatus() == NetworkState.Status.FAILED) {
                        errorMsg.setVisibility(View.VISIBLE);
                        errorMsg.setText(networkState.getMsg());
                    } else {
                        errorMsg.setVisibility(View.GONE);
                    }
                }
            };

        } else {
            throw new IllegalArgumentException("unknown view type");
        }
    }

    static abstract class AbstractViewHolder<T> extends RecyclerView.ViewHolder {
        private VenueListFragment.OnVenueSelectListener mListener;

        AbstractViewHolder(@NonNull View itemView) {
            super(itemView);
            init(itemView);
        }

        protected abstract void init(View itemView);

        void setListener(VenueListFragment.OnVenueSelectListener listener) {
            mListener = listener;
        }

        VenueListFragment.OnVenueSelectListener getListener() {
            return mListener;
        }

        public abstract void bind(T t, int position);
    }
}
