package ir.moazzin.bazaar.domain;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.paging.PagedList;

import ir.moazzin.bazaar.data.NetworkState;
import ir.moazzin.bazaar.model.Venue;
import ir.moazzin.bazaar.repo.GeneralRepository;

public class VenuesListViewModel extends AndroidViewModel {
    private GeneralRepository repository;

    public VenuesListViewModel(@NonNull Application application) {
        super(application);
        repository = GeneralRepository.getInstance(application);
    }

    public LiveData<PagedList<Venue>> getVenues(String location) {
        return repository.getPagedVenues(location);
    }

    public LiveData<NetworkState> getNetworkState() {
        return repository.getNetworkState();
    }
}
