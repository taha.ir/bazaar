package ir.moazzin.bazaar.domain;

import androidx.paging.PagedListAdapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import ir.moazzin.bazaar.R;
import ir.moazzin.bazaar.VenueListFragment.OnVenueSelectListener;
import ir.moazzin.bazaar.data.NetworkState;
import ir.moazzin.bazaar.model.Venue;
import ir.moazzin.bazaar.domain.ViewHolderFactory.AbstractViewHolder;

import static ir.moazzin.bazaar.model.Venue.DIFF_CALLBACK;

public class VenuesPagedListAdapter extends PagedListAdapter<Venue, AbstractViewHolder> {

    private NetworkState networkState;
    private final OnVenueSelectListener mListener;

    public VenuesPagedListAdapter(OnVenueSelectListener listener) {
        super(DIFF_CALLBACK);
        mListener = listener;
    }

    @Override
    public AbstractViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return ViewHolderFactory.create(layoutInflater, parent, viewType);
    }

    @Override
    public void onBindViewHolder(final AbstractViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case R.layout.fragment_venue_row:
                holder.setListener(mListener);
                holder.bind(getItem(position), position);
                break;
            case R.layout.network_state_row:
                holder.bind(networkState, position);
                break;
        }
    }

    private boolean hasExtraRow() {
        return networkState != null && networkState != NetworkState.LOADED;
    }

    @Override
    public int getItemViewType(int position) {
        if (hasExtraRow() && position == getItemCount() - 1) {
            return R.layout.network_state_row;
        } else {
            return R.layout.fragment_venue_row;
        }
    }

    public void setNetworkState(NetworkState newNetworkState) {
        NetworkState previousState = this.networkState;
        boolean previousExtraRow = hasExtraRow();
        this.networkState = newNetworkState;
        boolean newExtraRow = hasExtraRow();
        if (previousExtraRow != newExtraRow) {
            if (previousExtraRow) {
                notifyItemRemoved(getItemCount());
            } else {
                notifyItemInserted(getItemCount());
            }
        } else if (newExtraRow && previousState != newNetworkState) {
            notifyItemChanged(getItemCount() - 1);
        }
    }
}
