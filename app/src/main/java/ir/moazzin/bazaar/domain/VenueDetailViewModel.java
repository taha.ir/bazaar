package ir.moazzin.bazaar.domain;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import ir.moazzin.bazaar.model.Detail;
import ir.moazzin.bazaar.repo.GeneralRepository;

public class VenueDetailViewModel extends AndroidViewModel {
    private GeneralRepository repository;

    public VenueDetailViewModel(@NonNull Application application) {
        super(application);
        repository = GeneralRepository.getInstance((application));
    }

    public LiveData<Detail> getDetail(String venueId) {
        return repository.getDetail(venueId);
    }
}
