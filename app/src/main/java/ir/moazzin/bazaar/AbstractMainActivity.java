package ir.moazzin.bazaar;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public abstract class AbstractMainActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSIONS_CODE = 110;
    private boolean mFirstRequest = true;

    @Override
    protected void onResume() {
        super.onResume();
        if (!checkPermissions()) {
            requestPermissions();
        } else {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int fineState = checkSelfPermission(ACCESS_FINE_LOCATION);
            int coarseState = checkSelfPermission(ACCESS_COARSE_LOCATION);

            return fineState == PERMISSION_GRANTED && coarseState == PERMISSION_GRANTED;
        }
        return true;

    }

    /**
     * Start permissions requests.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermissions() {
        boolean fineRationalCheck = shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION);
        boolean coarseRationalCheck = shouldShowRequestPermissionRationale(ACCESS_COARSE_LOCATION);

        String[] permissions = new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};

        if (fineRationalCheck || coarseRationalCheck) {
            //یعنی در کل بار اول نیست که این سوال پرسیده میشود و چک باکس دیگر نپرس هم زده نشده
            showPermissionAlertDialog((dialog, which) ->
                    requestPermissions(permissions, REQUEST_PERMISSIONS_CODE));
        } else if (mFirstRequest) {
            //در کل یا در این اجرا بار اول است که درخواست داده می شود
            mFirstRequest = false;
            requestPermissions(permissions, REQUEST_PERMISSIONS_CODE);
        } else {
            //چون دفعه اول نیست که ریکوئست می دهیم حتما چک باکس دیگر نپرس را زده
            showPermissionAlertDialog((dialog, which) -> {
                Intent intent = new Intent();
                intent.setAction(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package",
                        BuildConfig.APPLICATION_ID, null);
                intent.setData(uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            });
        }
    }

    /**
     * Show Alert Dialog to finish activity or open app settings.
     */
    private void showPermissionAlertDialog(DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.permission_alert_title)
                .setMessage(R.string.permission_alert_message)
                .setCancelable(false)
                .setNegativeButton(R.string.cancel, (dialog, which) -> finish())
                .setPositiveButton(R.string.ok, okListener)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PERMISSION_GRANTED) {
                    //Permission granted
                    startLocationUpdates();
                } //else: Permission denied then request again in onResume when mFirstRequest is false
            } //else (grantResults.length == 0): Permission request is cancelled then request again in onResume.
        }
    }

    protected abstract void startLocationUpdates();

    protected abstract void stopLocationUpdates();

}
